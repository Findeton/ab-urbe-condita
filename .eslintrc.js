const path = require("path")

module.exports = {
    env: {
        browser: true,
        jest: true,
    },
    extends: [],
    parser: "@typescript-eslint/parser",
    plugins: ["@typescript-eslint", "react", "react-hooks"],
    rules: {
        "@typescript-eslint/await-thenable": "error",
        "@typescript-eslint/class-name-casing": "error",
        // '@typescript-eslint/consistent-type-assertions': ['error', {
        //     'assertionStyle': 'as',
        //     'objectLiteralTypeAssertions': 'allow-as-parameter'
        // }],
        "@typescript-eslint/consistent-type-definitions": ["error", "interface"],
        "@typescript-eslint/explicit-function-return-type": "error",
        "@typescript-eslint/explicit-member-accessibility": [
            "error",
            {
                accessibility: "explicit",
                overrides: {
                    constructors: "no-public",
                },
            },
        ],
        "@typescript-eslint/func-call-spacing": "error",
        "@typescript-eslint/interface-name-prefix": [
            "error",
            {
                prefixWithI: "always",
            },
        ],
        "@typescript-eslint/member-delimiter-style": [
            "error",
            {
                multiline: {
                    delimiter: "none",
                },
                singleline: {
                    delimiter: "semi",
                    requireLast: false,
                },
            },
        ],
        "@typescript-eslint/no-empty-function": "error",
        "@typescript-eslint/no-inferrable-types": [
            "warn",
            {
                ignoreParameters: true
            }
        ],
        "@typescript-eslint/no-misused-new": "error",
        "@typescript-eslint/no-misused-promises": [
            "error",
            {
                checksVoidReturn: false,
            },
        ],
        "@typescript-eslint/no-require-imports": "error",
        "@typescript-eslint/no-unnecessary-qualifier": "error",
        "@typescript-eslint/no-unnecessary-type-assertion": "error",
        "@typescript-eslint/no-unused-vars": [
            "error",
            {
                args: "after-used",
                argsIgnorePattern: "^_",
            },
        ],
        "@typescript-eslint/no-use-before-define": [
            "error",
            {
                classes: false,
            },
        ],
        "@typescript-eslint/no-useless-constructor": "error",
        "@typescript-eslint/no-var-requires": "error",
        "@typescript-eslint/prefer-regexp-exec": "error",
        "@typescript-eslint/promise-function-async": "error",
        "@typescript-eslint/require-array-sort-compare": "error",
        "@typescript-eslint/semi": ["error", "never"],
        // '@typescript-eslint/strict-boolean-expressions': ['error', {
        //     'ignoreRhs': true
        // }],
        "@typescript-eslint/type-annotation-spacing": "error",
        "@typescript-eslint/unbound-method": "error",
        "@typescript-eslint/unified-signatures": "warn",
        "eol-last": ["error", "always"],
        "func-call-spacing": "off",
        "indent": "off",
        "newline-before-return": "error",
        "no-empty-function": "off",
        "no-magic-numbers": "off",
        "no-multi-spaces": [
            "error",
            {
                ignoreEOLComments: true,
            },
        ],
        "no-new-wrappers": "error",
        "no-obj-calls": "error",
        "no-unused-expressions": [
            "error",
            {
                allowShortCircuit: true,
                allowTernary: true,
            },
        ],
        "no-unused-vars": "off",
        "no-var": "error",
        "quotes": ["error", "double"],
        "semi": "off",
        "react-hooks/rules-of-hooks": "error",
        // "react-hooks/exhaustive-deps": "warn",
        "react/jsx-uses-react": "error",
        "react/jsx-uses-vars": "error",
    },
    settings: {
        react: {
            version: "detect",
        },
    },
    root: true,
    parserOptions: {
        createDefaultProgram: true, 
        ecmaFeatures: {
            jsx: true,
        },
        ecmaVersion: 2020,
        project: path.resolve(__dirname, "tsconfig.json"),
        sourceType: "module",
    },
}
