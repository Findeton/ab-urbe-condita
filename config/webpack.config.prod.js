const merge = require("webpack-merge")
const common = require("./webpack.config.common")
const HtmlWebPackPlugin = require("html-webpack-plugin")
const path = require("path")

module.exports = merge(common, {
    mode: "production",
    devtool: "source-map",
    plugins: [
        new HtmlWebPackPlugin({
           template: path.resolve( __dirname, '../public/index.html' ),
           filename: 'index.html'
        })
    ],
    output: {
        publicPath: "https://aburbecondita.com/"
    }
})
