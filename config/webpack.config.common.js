const {CleanWebpackPlugin} = require("clean-webpack-plugin")
const path = require("path")

module.exports = {
    entry: "./src/index.tsx",
    //externals: {
    //    "react": "React",
    //    "react-dom": "ReactDOM"
    //},
    module: {
        rules: [
            {
                test: /\.ts(x?)$/,
                exclude: /node_modules/,
                use: [{loader: "ts-loader"}],
            },
            {
                test: /\.s[ac]ss$/,
                use: [
                    // Creates `style` nodes from JS strings
                    "style-loader",
                    // Translates CSS into CommonJS
                    "css-loader",
                    // Compiles Sass to CSS
                    "sass-loader",
                ],
            },
            {
                test: /\.(png|gif|jpg|jpeg|ico|eot|ttf|woff|woff2|svg)$/,
                use: ["file-loader"],
            },
        ],
    },
    output: {
        filename: `bundle.${process.env.SOURCE_VERSION}.js`,
        path: path.resolve(__dirname, "..", "dist"),
        publicPath: "http://localhost:8081/",
    },
    plugins: [new CleanWebpackPlugin()],
    resolve: {
        extensions: [".js", ".ts", ".tsx"],
    },
}
