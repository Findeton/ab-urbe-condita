const merge = require("webpack-merge")
const common = require("./webpack.config.common")
const ForkTsCheckerWebpackPlugin = require("fork-ts-checker-webpack-plugin")
const HtmlWebPackPlugin = require("html-webpack-plugin")
const path = require("path")
const webpack = require('webpack')

module.exports = merge.smart(common, {
    mode: "development",
    devtool: "inline-source-map",
    watchOptions: {
        poll: true,
        ignored: /node_modules/
    },
    module: {
        rules: [
            {
                test: /\.ts(x?)$/,
                exclude: /node_modules/,
                use: [
                    {
                        loader: "ts-loader",
                        options: {transpileOnly: true},
                    },
                ],
            },
        ],
    },
    devServer: {
        port: 8081,
        sockHost: "localhost:8081",
        disableHostCheck: true,
        liveReload: true,
        inline: true,
        headers: {
            "Access-Control-Allow-Origin": "*",
            "Access-Control-Allow-Methods": "GET, POST, PUT, DELETE, PATCH, OPTIONS",
        },
    },
    plugins: [
        new HtmlWebPackPlugin({
           template: path.resolve( __dirname, '../public/index.html' ),
           filename: 'index.html'
        }),
        new webpack.HotModuleReplacementPlugin(),
        new ForkTsCheckerWebpackPlugin()
    ],
})
