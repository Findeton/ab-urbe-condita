# ab-urbe-condita

Code for aburbecondita.com

# Instructions

Install dependencies

    yarn

Run transpiler

    npx tsc -w

Serve web in development and visit http://localhost:8081/

    yarn run serve

Build for production

    yarn run build