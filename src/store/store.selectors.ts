import {IAppStore} from "../types/store/IAppStore"
import {IYearFormats} from "../types/YearFormats"

export const selectCurrent = (state: IAppStore): IYearFormats => state.current

export const selectCalculator = (state: IAppStore): IYearFormats => state.calculator
