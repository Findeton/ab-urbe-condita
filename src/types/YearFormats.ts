export interface IYearFormats {
    ad: number
    auc: number
    roman_numerals: string
}
